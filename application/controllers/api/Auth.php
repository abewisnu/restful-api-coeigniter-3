<?php

defined('BASEPATH') or exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Auth extends BD_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');
    }

    public function login_post()
    {
        $username = $this->post('username'); //Username Posted
        $password = sha1($this->post('password')); //Pasword Posted
        $getUser = array('username' => $username); //For where query condition
        $key = $this->config->item('thekey');
        $invalidLogin = ['status' => 'Invalid Login']; //Respon if login invalid
        $val = $this->M_main->get_user($getUser)->row(); //Model to get single data row from database base on username
        if ($this->M_main->get_user($getUser)->num_rows() == 0) {
            $this->response($invalidLogin, REST_Controller::HTTP_NOT_FOUND);
        }
        $match = $val->password;   //Get password for user from database
        if ($password == $match) {  //Condition if password matched
            $date = new DateTime();
            $session['id'] = $val->id;  //From here
            $session['user'] = $val->user;
            $session['username'] = $username;
            $session['level'] = $val->level;
            $session['iat'] = $date->getTimestamp();
            //$session['exp'] = $date->getTimestamp() + 60 * 1; // 1 Menit
            $session['exp'] = $date->getTimestamp() + 60 * 60 * 20; //To here is to generate token
            //$session['exp'] = $date->getTimestamp() + 60 * 60 * 24 * 60; //To here is to generate token
            $this->response([
                'status' => TRUE,
                'message' => 'Login Success',
                'user' => $session,
                'token' =>$output['token'] = JWT::encode($session, $key) //This is the output token
            ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Login Invalid',
            ], REST_Controller::HTTP_NOT_FOUND);  //This is the respon if failed
        }
    }
}
