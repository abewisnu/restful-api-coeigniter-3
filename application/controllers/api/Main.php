<?php
defined('BASEPATH') or exit('No direct script access allowed');
// REST_Controller::HTTP_OK = OK (200) being the HTTP response code
// REST_Controller::HTTP_NOT_FOUND = NOT_FOUND (404) being the HTTP response code
// REST_Controller::HTTP_NO_CONTENT = NO_CONTENT (204) being the HTTP response code
// REST_Controller::HTTP_BAD_REQUEST = BAD_REQUEST (400) being the HTTP response code
class Main extends BD_Controller
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->auth();
    }

    public function session_post()
    {

        $getSession = $this->user_data;
        $this->response($getSession, 200); // OK (200) being the HTTP response code

    }

    public function users_post()
    {
        $id = $this->post('id'); // Get id parameter

        if ($id === NULL) { // Check if parameter id null or blank
            $users = $this->db->get('user'); // Get all users query
            if ($users) {
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User Found',
                    'data' => json_decode(json_encode($users->result()))
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'User Not Found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $users = $this->db->get_where('user', array('id' => $id)); // Get all users query
            if ($users) {
                $this->response([
                    'status' => TRUE,
                    'message' => 'User Found',
                    'data' => json_decode(json_encode($users->result()))
                ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

            } else {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'User Not Found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
}
