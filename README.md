# Restful Api Codeigniter 3.x
This is an example of making restful api with codeigniter 3 used for learning or as a master of initial creation of restful api with PHP

## Git Clone
you can clone this source code with the command

```bash
git clone git@gitlab.com:abewisnu/restful-api-coeigniter-3.git 
```

## Mysql Database
Create table login :
```sql
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` enum('admin','user') DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `log` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
```
Create table user :
```sql
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `log` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
```
## Login
Add this value to the login table in the username and password example :
```
username : admin
password : d033e22ae348aeb5660fc2140aec35850c4da997
```
this is sha1 in the encryption is worth
admin is d033e22ae348aeb5660fc2140aec35850c4da997
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)