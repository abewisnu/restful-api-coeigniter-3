<?php return array(
    'root' => array(
        'name' => 'chriskacerguis/codeigniter-restserver',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'c8df69647c29f9bfc41dde47b802c8eac8a8dd28',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'chriskacerguis/codeigniter-restserver' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'c8df69647c29f9bfc41dde47b802c8eac8a8dd28',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'codeigniter/framework' => array(
            'pretty_version' => '3.1.13',
            'version' => '3.1.13.0',
            'reference' => 'bcb17eb8ba53a85de154439d0ab8ff1bed047bc9',
            'type' => 'project',
            'install_path' => __DIR__ . '/../codeigniter/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
